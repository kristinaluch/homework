package main.com.firsthomework;

public class FunctionHomework {
    public void printAllResults(){
        System.out.println("�������");
        int dayNum = 6;
        System.out.println("1.��� ������� "+dayNum+" ���� ������ "+ getNameDayOfWeek(dayNum));
        //2
        System.out.println("2.��������� ����� �������: "+ getDistanceBetweenPoints(1, 4, 3, 5));
        //3
        System.out.println("3.����� �������� " + printNumberToWord(107));
        //4
        System.out.println("4. ����� (�����): "+ printNumberToInteger("��� ����"));


    }
    //1
    public String getNameDayOfWeek(int dayNum){
        String[] dayName = {"", "�����������", "�������", "�����", "�������", "�������", "�������", "�����������"};
        if (dayNum>0&&dayNum<=7)
            return dayName[dayNum];
        else return "... �� ��� ����� ������...?";
    }

    //2
    public int getDistanceBetweenPoints(int xA, int yA, int xB, int yB){
        int distance = (int) Math.sqrt(Math.pow((xA-xB), 2)+Math.pow((yA-yB), 2));
        return distance;

    }
    //������ �����(0-999), �������� ������ � �������� �����.
    public String printNumberToWord(int numberToWord) {

        if (numberToWord < 0)
            return "��������� ����� ������ ���� �������������";

        if (numberToWord == 0) {
            return "����";
        }
        String numbers = "" + numberToWord;

        int numLen =  numbers.length();

        if (numLen>3){
            return "��������� ����� �� ������ � �������� 0-999";
        }

        String resultName = "";
        int ind;
        String[] name1 = {"", "����", "���", "���", "������", "����", "�����", "����", "������", "������"};

        if (numLen==1){
            ind = numberToWord;
            resultName = name1[ind];
            return resultName;
        }

        int[] num = new int[numLen];

        for (int i = 0; i<numLen; i++) {
            num[i] = numberToWord % 10;
            numberToWord = numberToWord / 10;
        }
        String[] arrayName = new String[numLen];

        String[] name10 = {"������", "�����������", "����������", "����������", "������������",
                "����������", "�����������", "����������", "������������", "�������������"};
        String[] name20 = {"", "", "��������", "��������", "�����","���������", "����������",
                "���������", "�����������", "���������"};
        String[] name100 = {"", "���", "������", "������", "���������", "�������", "��������",
                "�������", "���������", "���������"};

        // [x] - *10^x
        //units

        if (num[1] != 1) {
            ind = num[0];
            arrayName[0] = name1[ind];
        }
        if (num[1] == 1) {
            ind = num[0];
            arrayName[1] = name10[ind];
        }

        if (num[1] >= 2||num[1]==0) {
            ind = num[1];
            arrayName[1] = name20[ind];
        }

        if (numLen > 2 ) {
            //if(num[2] > 0) {
                ind = num[2];
                arrayName[2] = name100[ind];
            //}
        }
        if (numLen == 3) {
            resultName = arrayName[2] + " " + arrayName[1] + " " + arrayName[0];
        } else if (numLen == 2) {
            resultName = arrayName[1] + " " + arrayName[0];
        }

        return resultName;

    }


    public int printNumberToInteger(String printNumber){
        if (printNumber.equalsIgnoreCase("����"))
            return 0;
        if (printNumber.startsWith("�����")){
            return -1;
        }
        String [] arrayStr = printNumber.split("\\s");
        int numLen = arrayStr.length;


        String[] name = {"����", "���", "���", "������", "����", "�����", "����", "������", "������", "������", "�����������", "����������", "����������", "������������",
                "����������", "�����������", "����������", "������������", "�������������", "��������", "��������", "�����","���������", "����������",
                "���������", "�����������", "���������", "���", "������", "������", "���������", "�������", "��������",
                "�������", "���������", "���������"};
        int [] num = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900};
        String s;
        int n;
        int result = 0;
        for (String value : arrayStr) {
            s = value;
            for (int j = 0; j < name.length; j++) {
                if (s.equalsIgnoreCase(name[j])) {
                    n = num[j];
                    result = n + result;
                }
            }
        }


        return result;
    }



}
