package main.com.firsthomework;

import java.util.Arrays;

public class ArrayHomework {


    //�������
    public void printAllResult() {
        System.out.println("�������");
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        //1
        System.out.println("1.����������� ����� � ������� " + findMinNumInArray(array));
        //2
        System.out.println("2.������������ ����� � ������� " + findMaxNumInArray(array));
        //3
        System.out.println("3.������ ������������ ����� � ������� " + getIndexMin(array));
        //4
        System.out.println("4.������ ������������� ����� � ������� " + getIndexMax(array));
        //5.
        System.out.println("5.����� ��������� ������� � ��������� ��������� " + getSumOddIndex(array));
        //6. ������� ������ �������
        int[] reverseArray = getReverse(array);
        System.out.println("6.������ ������� " + Arrays.toString(reverseArray));
        //7.
        System.out.println("7.���������� �������� ��������� ������� " + getOddElementsArray(array));
        //8.
        System.out.println("8. �������� ������� ������ � ������ �������� �������. " +
                "������: "+ Arrays.toString(array) + " ���������: "
                + Arrays.toString(swapHalvesOfArray(array)));

        //9. ������������� ������ (��������� (Bubble), ������� (Select), ��������� (Insert))
        int[] arrayForSort = {6, 3, 89, 51, 44, 2, 0, 77};
        System.out.println("9.������ ��� ���������� "+Arrays.toString(arrayForSort));
        //9.1 Bubble
        System.out.println("9.1.������, ��������������� \"���������\" "+Arrays.toString(sortBubble(arrayForSort)));
        //9.2
        System.out.println("9.2.������, ��������������� \"�������\" "+Arrays.toString(sortSelect(arrayForSort)));
        //9.3
        System.out.println("9.3.������, ��������������� \"���������\" "+Arrays.toString(sortInsert(arrayForSort)));
        //10.  ������������� ������ (Quick, Merge, Shell, Heap)
    }
    //1
    public int findMinNumInArray(int[] a) {
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            if (a[i] <= min)
                min = a[i];
        }
        return min;
    }
    //2
    public int findMaxNumInArray(int[] a) {
        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            max = Math.max(a[i], max);
        }
        return max;
    }
    //3
    public int getIndexMin(int[] a) {
        int indexMin = 0;
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            if (a[i] < min) {
                min = a[i];
                indexMin = i;
            }
        }
        return indexMin;
    }
    //4
    public int getIndexMax(int[] a) {
        int indexMax = 0;

        int max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (a[i] > max) {
                max = a[i];
                indexMax = i;
            }
        }
        return indexMax;
    }
    //5
    public int getSumOddIndex(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            if (i % 2 == 1)
                sum = sum + a[i];
        }
        return sum;
    }
    //6
    public int[] getReverse(int[] a) {
        int n = a.length - 1;
        int change;
        for (int i = 0; i < a.length / 2; i++) {
            change = a[i];
            a[i] = a[n - i];
            a[n - i] = change;
        }
        return a;
    }
    //7
    public int getOddElementsArray(int[] a) {
        int oddElements = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 1)
                oddElements = oddElements + 1;
        }
        return oddElements;
    }
    //8. �������� ������� ������ � ������ �������� �������
    public int[] swapHalvesOfArray(int [] array){
        int half = array.length/2;
        int swap;
        if(array.length%2==1){
            half = array.length/2 +1;
            for (int i = 0;i<(array.length/2);i++){
                // array[i+half] = beginArray[i];
                //array[i] = endArray[i];
                swap = array[i];
                array[i] = array[half+i];
                array[half+i]=swap;
            }
        } else {
            for (int i = 0; i < half; i++) {

                swap = array[i];
                array[i] = array[half + i];
                array[half + i] = swap;
            }
        }
            return array;
    }
    //9
    //9.1
    public int[] sortBubble(int[] array) {
        boolean needIteration = true;
        while(needIteration){
            needIteration = false;
            for (int i = 1; i < array.length; i++){
                if (array[i] < array[i-1]){
                    swap(array, i, i-1);
                    needIteration = true;
                }
            }
        }
        return array;
    }

    private void swap (int[] a, int ind1, int ind2) {
        int tmp = a[ind1];
        a[ind1] = a[ind2];
        a[ind2] = tmp;
    }
    //9.2
    public int[] sortSelect(int[] array){
        int minInd;
        int minEl;
        for (int left = 0; left < array.length; left++){
            minInd = left;
            minEl = array[minInd];
            for (int i = left; i < array.length; i++){
                if (array[i]<minEl){
                    minInd = i;
                    minEl = array[i];
                    swap (array, left, minInd);
                }
            }
        }
        return array;
    }
    //9.3
    public int[] sortInsert(int[] array){
        for (int i = 1; i < array.length; i++){
            for (int j = i; j >0 && array[j-1] > array[j]; j--){
                swap(array, (j-1), j);
            }
        }
        return array;
    }

}
