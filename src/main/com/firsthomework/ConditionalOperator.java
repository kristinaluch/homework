package main.com.firsthomework;

public class ConditionalOperator {
    public void printResult(){
        System.out.println("�������� ���������");
        //1
        System.out.println("1. ��������� ����� "+ checkCondition(3, 5));
        //2
        System.out.println("2. ���������������� � ��������: " + getQuarter(-9, 5));
        //3
        System.out.println("3. ����� ������������� ����� " + getPositiveNumSum(7, -80, 1));
        //4
        System.out.println("4. ��������� ���� " + getMaxNum(0, 2, 7));
        //5
        System.out.println("5. ������ ��������: " + getRating(76));
    }

    public int checkCondition(int a, int b) {
        if (a % 2 == 0)
            return (a * b);
        else return (a + b);
    }

    public int getQuarter(int x, int y) {
        if (x > 0) { //x+
            if (y > 0) //y+
                return 1;
            else if (y < 0) return 4;
            else return 0;
        } else if (x < 0) {
            if (y > 0) //y+
                return 2;
            else if (y < 0) return 3;
            else return 0;
        } else return 0;

    }

    public int getPositiveNumSum(int a, int b, int c) {
        if (a >= 0)
            if (b >= 0)
                if (c >= 0)
                    return a + b + c;
                else return a + b;
            else if (c >= 0)
                return a + c;
            else return a;
        else if (b >= 0)
            if (c >= 0)
                return b + c;
            else return b;
        else if (c >= 0)
            return c;
        else return 0;
    }

    public static int getMaxNum(int a, int b, int c) {
        if ((a * b * c) > (a + b + c))
            return (a * b * c) + 3;
        else return (a + b + c) + 3;
    }

    public String getRating(int score) {
        if (score <= 19)
            return "F";
        else if (score <= 39)
            return "E";
        else if (score <= 59)
            return "D";
        else if (score <= 74)
            return "C";
        else if (score <= 89)
            return "B";
        else if (score <= 100)
            return "A";
        else return "� �� ���� � ������� �� ����������!";
    }

}
