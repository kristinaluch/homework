package main.com.firsthomework;

public class Cycles {
    //�����

    public void printAllResults(){
        System.out.println("�����");
        //1
        System.out.println(getSumEvenNumber());
        //2 (���� ������,���� �����)
        System.out.println("2. ������� �� �����? "+ getSimpleNum(5));
        //3 ��������
        int numberInPower = 147;
        System.out.println("3.1 ������ ����� "+ numberInPower +" ����� " + getRootOfNumber(numberInPower));
        System.out.println("3.2 ������ ����� (�������� �����) "+ numberInPower +" ����� " + getRootBinarySearch(numberInPower));
        //4
        int n = 5;
        System.out.println("4. ��������� ����� " + n + " ����� " + getFactorialOfNum(n));
        //5
        int numDig = 762;
        System.out.println("5.����� ���� ����� " + numDig +" ����� " + getDigitSum(numDig));
        //6
        int numMir = 457;
        System.out.println("6. �����: "+numMir+", ���������� ��������� "+ getNumMirror(numMir));
    }
    //1
    public String getSumEvenNumber() {
        int sum = 0;
        int n = 0;
        for (int i = 1; i < 100; i++) {
            if (i % 2 == 0) {
                sum = sum + i;
                n = n + 1;
            }
        }
        String result = "����������� ������ ������ � ��������� �� 1 �� 99 ����� " + n + "," +
                " � ����� ���� ����� ��������� " + sum;
        return result;
    }
    //2
    public String getSimpleNum(int n) {
        String simple = ("����� " + n + " �������� ������� ������");
        String notSimple = ("����� " + n + " �� �������� ������� ������");
        int posN = Math.abs(n);
        int x;
        if (posN >= 2)
            for (int i = 2; i < posN - 1; i++) {
                x = posN % i;
                if (x == 0)
                    return notSimple;
            }
        return simple;
    }

    //3
    //3.1
    public int getRootOfNumber(int numberInPower){
        numberInPower = Math.abs(numberInPower);
        int sq;
        for(int i = 0;;i++){
            sq = i*i;
            if (sq > numberInPower) {
                return i-1;
            }
        }

    }
    //3.2
    public int getRootBinarySearch(int numberInPower){
        numberInPower = Math.abs(numberInPower);
        double low = 0.0;
        double max = numberInPower;
        double mid;
        double sq;
        for (;;) {
            mid = (low+max)/2;
            sq = mid*mid;
            if (sq < numberInPower) {
                low = mid;
                continue;
            }
            if (sq > numberInPower) {
                max = mid;
                continue;
            }
            return (int)mid;
        }

    }

    //4
    public int getFactorialOfNum(int n) {
        int factorialN = 1;
        for (int i = 1; i <= n; i++) {
            factorialN = factorialN * i;
        }
        return factorialN;
    }
    // ����� 5
    public int getDigitSum(int number){
        int sumOfDigit = 0;
        int numberMod = Math.abs(number);
        int digit;
        while (numberMod > 0){
            digit = numberMod%10;
            sumOfDigit = sumOfDigit + digit;
            numberMod = numberMod/10;
        }
        if (number<0){
            sumOfDigit = sumOfDigit*(-1);
        }
        return sumOfDigit;
    }
    //����� 6
    public int getNumMirror(int number){
        int digit;
        int mirrorNum = 0;
        while (number > 0){
            digit = number%10;
            mirrorNum = mirrorNum * 10 + digit;
            number = number/10;
        }
        return mirrorNum;
    }
}
