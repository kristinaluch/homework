package test.com.firsthomework;

import main.com.firsthomework.ArrayHomework;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ArrayHomeworkTest {

    ArrayHomework cut = new ArrayHomework();

    static Arguments[] findMinNumInArrayTestArgs(){
        return new Arguments[]{
                Arguments.arguments(7, new int[]{69, 9, 70, 7}),
                Arguments.arguments(-12, new  int[]{-1, 43,7,-12}),
        };
    }

    @ParameterizedTest
    @MethodSource("findMinNumInArrayTestArgs")
    void findMinNumInArrayTest(int expected, int[] array){
        int actual = cut.findMinNumInArray(array);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] findMaxNumInArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(70, new int[]{69, 9, 70, 7}),
                Arguments.arguments(-12, new  int[]{-100, -43,-21,-12}),
        };
    }

    @ParameterizedTest
    @MethodSource("findMaxNumInArrayTestArgs")
    void findMaxNumInArrayTest(int expected, int[] array){
        int actual = cut.findMaxNumInArray(array);
        Assertions.assertEquals(expected,actual);
    }

    /////////////////////////////////
    static Arguments[] getIndexMinTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, new int[]{69, 9, 70, 7}),
                Arguments.arguments(0, new  int[]{-100, -43,-21,-12})
        };
    }

    @ParameterizedTest
    @MethodSource("getIndexMinTestArgs")
    void getIndexMin(int expected, int[] array){
        int actual = cut.getIndexMin(array);
        Assertions.assertEquals(expected,actual);
    }
    static Arguments[] getIndexMaxTestArgs(){
        return new Arguments[]{
                Arguments.arguments(5, new int[]{69, 9, 70, 7, -9, 666}),
                Arguments.arguments(0, new  int[]{-10, -43,-21,-12})
        };
    }

    @ParameterizedTest
    @MethodSource("getIndexMaxTestArgs")
    void getIndexMax(int expected, int[] array){
        int actual = cut.getIndexMax(array);
        Assertions.assertEquals(expected,actual);
    }
    static Arguments[] getSumOddIndexTestArgs(){
        return new Arguments[]{
                Arguments.arguments(615, new int[]{99, 10, 7, 5, -9, 600}),
                Arguments.arguments(-52, new  int[]{-10, -40,-21,-12})
        };
    }

    @ParameterizedTest
    @MethodSource("getSumOddIndexTestArgs")
    void getSumOddIndex(int expected, int[] array){
        int actual = cut.getSumOddIndex(array);
        Assertions.assertEquals(expected,actual);
    }
    static Arguments[] getReverseTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{1,2,3}, new int[]{3,2,1}),
                Arguments.arguments(new int[]{9,8,7}, new int[]{7,8,9})
        };
    }

    @ParameterizedTest
    @MethodSource("getReverseTestArgs")
    void getReverse(int[] expected, int[] array){
        int[] actual = cut.getReverse(array);
        Assertions.assertArrayEquals(expected,actual);
    }

    static Arguments[] getOddElementsArrayTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, new int[]{7,9,2,1}),
                Arguments.arguments(0, new int[]{4,6,2,0}),
        };
    }

    @ParameterizedTest
    @MethodSource("getOddElementsArrayTestArgs")
    void getOddElementsArray(int expected, int[] array){
        int actual = cut.getOddElementsArray(array);
        Assertions.assertEquals(expected,actual);
    }
    static Arguments[] swapHalvesOfArrayTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{1,2,3,4}, new int[]{3,4,1,2}),
                Arguments.arguments(new int[]{4,5,3,1,2}, new int[]{1,2,3,4,5}),
        };
    }

    @ParameterizedTest
    @MethodSource("swapHalvesOfArrayTestArgs")
    void swapHalvesOfArray(int[] expected, int[] array){
        int[] actual = cut.swapHalvesOfArray(array);
        Assertions.assertArrayEquals(expected,actual);
    }

    static Arguments[] sortTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{5,6,7,8,9}, new int[]{9,6,7,8,5}),
                Arguments.arguments(new int[]{1,2,3,4,5}, new int[]{3,5,4,1,2}),
        };
    }

    @ParameterizedTest
    @MethodSource("sortTestArgs")
    void sortBubble(int[] expected, int[] array){
        int[] actual = cut.sortBubble(array);
        Assertions.assertArrayEquals(expected,actual);
    }

    @ParameterizedTest
    @MethodSource("sortTestArgs")
    void sortSelect(int[] expected, int[] array){
        int[] actual = cut.sortSelect(array);
        Assertions.assertArrayEquals(expected,actual);
    }

    @ParameterizedTest
    @MethodSource("sortTestArgs")
    void sortInsert(int[] expected, int[] array){
        int[] actual = cut.sortInsert(array);
        Assertions.assertArrayEquals(expected,actual);
    }


}
