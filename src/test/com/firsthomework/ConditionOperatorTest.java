package test.com.firsthomework;
import main.com.firsthomework.ConditionalOperator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


public class ConditionOperatorTest {


    ConditionalOperator cut = new ConditionalOperator();

    static Arguments[] checkConditionTestArgs() {
        return new Arguments[]{
                    Arguments.arguments(6, 2, 3),
                    Arguments.arguments(5, 3, 2)
            };
        }

    @ParameterizedTest
    @MethodSource("checkConditionTestArgs")
    void checkConditionTest(int expected, int valueEvenOdd, int valueB){
        int actual = cut.checkCondition(valueEvenOdd, valueB);
        Assertions.assertEquals(expected, actual);
        }

    static Arguments[] getQuarterTestArgs() {
        return new Arguments[]{
            Arguments.arguments(1,4,5),
            Arguments.arguments(2, -9, 3),
            Arguments.arguments(3, -8, -10),
            Arguments.arguments(4, 9, -3),
            Arguments.arguments(0, 0, -10),
            Arguments.arguments(0, 9, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("getQuarterTestArgs")
    void getQuarterTest(int expected, int pointX, int pointY){
        int actual = cut.getQuarter(pointX, pointY);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getPositiveNumSumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(13, 7, -90, 6),
                Arguments.arguments(29, -5, -9, 29),
                Arguments.arguments(10, 5, 2, 3),
                Arguments.arguments(0, -1, -4, -7)
        };
    }

    @ParameterizedTest
    @MethodSource("getPositiveNumSumTestArgs")
    void getPositiveNumSumTest(int expected, int numOne, int numTwo, int numThree){
        int actual = cut.getPositiveNumSum(numOne, numTwo, numThree);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getMaxNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(23, 1, 5, 4),
                Arguments.arguments(17, 7, 8, -1),
        };
    }

    @ParameterizedTest
    @MethodSource("getMaxNumTestArgs")
    void getMaxNumTest(int expected, int numOne, int numTwo, int numThree){
        int actual = cut.getMaxNum(numOne, numTwo, numThree);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getRatingTestArgs(){
        return new Arguments[]{
          Arguments.arguments("F", 17),
          Arguments.arguments("E", 25),
          Arguments.arguments("D", 57),
          Arguments.arguments("C", 74),
          Arguments.arguments("B", 81),
          Arguments.arguments("A", 90)
        };
    }

    @ParameterizedTest
    @MethodSource("getRatingTestArgs")
    void getRatingTest(String expected, int score){
        String actual = cut.getRating(score);
        Assertions.assertEquals(expected, actual);
    }

}
