package test.com.firsthomework;

import main.com.firsthomework.Cycles;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CyclesTest {

    Cycles cut = new Cycles();

    @Test
    void getSumEvenNumberTest(){
        String expected = "����������� ������ ������ � ��������� �� 1 �� 99 ����� " + 49 + "," +
                " � ����� ���� ����� ��������� " + 2450;
        String actual = cut.getSumEvenNumber();
        Arguments.arguments(expected, actual);
    }

    static Arguments[] geSimpleNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments("����� 5 �������� ������� ������", 5),
                Arguments.arguments("����� 0 �������� ������� ������", 0),
                Arguments.arguments("����� 12 �� �������� ������� ������", 12)


        };
    }

    @ParameterizedTest
    @MethodSource("geSimpleNumTestArgs")
    void geSimpleNumTest(String expected, int number){
        String actual = cut.getSimpleNum(number);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getRootOfNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 16),
                Arguments.arguments(0, 0),
                Arguments.arguments(3, 10)
        };
    }

    @ParameterizedTest
    @MethodSource("getRootOfNumberTestArgs")
    void getRootOfNumberTest(int expected, int numberInPower){
        int actual = cut.getRootOfNumber(numberInPower);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] getRootBinarySearchTestArgs(){
        return new Arguments[]{
                Arguments.arguments(4, 16),
                Arguments.arguments(0, 0),
                Arguments.arguments(3, 10)
        };
    }

    @ParameterizedTest
    @MethodSource("getRootBinarySearchTestArgs")
    void getRootBinarySearchTest(int expected, int numberInPower){
        int actual = cut.getRootBinarySearch(numberInPower);
        Assertions.assertEquals(expected,actual);
    }


    static Arguments[] getFactorialOfNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(720,6),
                Arguments.arguments(1, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("getFactorialOfNumTestArgs")
    void getFactorialOfNumTest(int expected, int number){
        int actual = cut.getFactorialOfNum(number);
        Assertions.assertEquals(expected,actual);
    }


    static Arguments[] getDigitSumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(15, 456),
                Arguments.arguments(0, 0),
                Arguments.arguments(-2, -101)

        };
    }

    @ParameterizedTest
    @MethodSource("getDigitSumTestArgs")
    void getDigitSumTest(int expected, int number){
        int actual = cut.getDigitSum(number);
        Assertions.assertEquals(expected,actual);
    }


    static Arguments[] getNumMirrorTestArgs(){
        return new Arguments[]{
                Arguments.arguments(654, 456),
                Arguments.arguments(0, 0),
                Arguments.arguments(51,15)

        };
    }

    @ParameterizedTest
    @MethodSource("getNumMirrorTestArgs")
    void getNumMirrorTest(int expected, int number){
        int actual = cut.getNumMirror(number);
        Assertions.assertEquals(expected,actual);
    }



}
