package test.com.firsthomework;

import main.com.firsthomework.FunctionHomework;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class FunctionHomeworkTest {

    FunctionHomework cut = new FunctionHomework();

    static Arguments[] getNameDayOfWeekTestArgs(){
        return new Arguments[]{
                Arguments.arguments("�����������", 1),
                Arguments.arguments("�������",2),
                Arguments.arguments("�����",3),
                Arguments.arguments("�������",4),
                Arguments.arguments("�������", 5),
                Arguments.arguments("�������", 6),
                Arguments.arguments("�����������", 7),
                Arguments.arguments("... �� ��� ����� ������...?", 8)
        };
    }

    @ParameterizedTest
    @MethodSource("getNameDayOfWeekTestArgs")
    void getNameDayOfWeek(String expected, int numberDayOfWeek){
        String actual = cut.getNameDayOfWeek(numberDayOfWeek);
        Assertions.assertEquals(expected,actual);
    }
    static Arguments[] getDistanceBetweenPointsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2, 3, 2, 1, 2),
                Arguments.arguments(0, 1,1, 1, 1),
        };
    }

    @ParameterizedTest
    @MethodSource("getDistanceBetweenPointsTestArgs")
    void getDistanceBetweenPoints(int expected, int xA, int yA, int xB, int yB){
        double actual = cut.getDistanceBetweenPoints(xA,yA,xB,yB);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] printNumberToWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("��������� ��������� ������", 999),
                Arguments.arguments("����", 0),
                Arguments.arguments("����", 5),
                Arguments.arguments("��������� ", 50),
                Arguments.arguments("���  ", 100),
                Arguments.arguments("��������� ����� �� ������ � �������� 0-999", 1000),
                Arguments.arguments("��������� ����� ������ ���� �������������", -5)
        };
    }

    @ParameterizedTest
    @MethodSource("printNumberToWordTestArgs")
    void printNumberToWord(String expected, int numberToWord){
        String actual = cut.printNumberToWord(numberToWord);
        Assertions.assertEquals(expected,actual);
    }
    static Arguments[] printNumberToIntegerTestArgs(){
        return new Arguments[]{
                Arguments.arguments(999, "��������� ��������� ������"),
                Arguments.arguments(0, "����"),
                Arguments.arguments(5, "����"),
                Arguments.arguments(50, "���������"),
                Arguments.arguments(100, "���"),
                Arguments.arguments(0, "������"),
                Arguments.arguments(-1, "����� ����")
        };
    }

    @ParameterizedTest
    @MethodSource("printNumberToIntegerTestArgs")
    void printNumberToInteger(int expected, String printNumber){
        int actual = cut.printNumberToInteger(printNumber);
        Assertions.assertEquals(expected,actual);
    }

}
